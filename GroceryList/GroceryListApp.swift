//
//  GroceryListApp.swift
//  GroceryList
//
//  Created by vikingosegundo on 08/02/2023.
//

import SwiftUI

@main
struct GroceryListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(groceries:groceryList)
        }
    }
}
//fileprivate var coreDataManager = CoreDataManager()
fileprivate var store      : Store                 = createDiskStore()
fileprivate var groceryList: GroceryListViewModel  = GroceryListViewModel(store:store, roothandler:{ rootHandler($0) })
fileprivate var rootHandler: ((Message) -> ())!    = createAppDomain(
    store      : store,
    receivers  : [ groceryList.handle(msg:) ],
    rootHandler: { rootHandler($0) }
)
