//
//  Message.swift
//  GroceryList
//
//  Created by vikingosegundo on 08/02/2023.
//

enum Message {
    case groceries(Groceries)
    public enum Groceries {
        case cmd(CMD)
        case ack(ACK)
        enum CMD {
            case add    (item:ListItem)
            case remove (item:ListItem)
        }
        enum ACK {
            case added  (item:ListItem)
            case removed(item:ListItem)
        }
    }
}
