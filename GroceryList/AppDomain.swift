//
//  AppDomain.swift
//  GroceryList
//
//  Created by vikingosegundo on 08/02/2023.
//

import Foundation

typealias  Input = (Message) -> ()
typealias Output = (Message) -> ()

func createAppDomain(
    store      : Store<AppState,AppState.Change>,
    receivers  : [Input],
    rootHandler: @escaping Output
) -> Input
{
    let features: [Input] = [
       createGroceryListFeature(store:store,output:rootHandler)
    ]
    
    return { msg in
        (receivers + features).forEach {
            $0(msg)
        }
    }
    
}
