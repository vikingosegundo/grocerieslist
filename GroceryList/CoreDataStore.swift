////
////  CoreDataStore.swift
////  GroceryList
////
////  Created by vikingosegundo on 08/02/2023.
////
//
//import Foundation.NSFileManager
//import CoreData
//
//
//final class CoreDataManager: NSObject {
//    let container: NSPersistentContainer = NSPersistentContainer(name: "MainqModel")
//    var groceryList: GroceryList = .init()
//    
//    override init() {
//            super.init()
//            container.loadPersistentStores { _, _ in }
//        }
//}
//
//
//func createCoreDataStore(
//    manager:CoreDataManager
//) -> Store<AppState, AppState.Change>
//{
//
//    var s = loadAppState(with:manager) { didSet { c.forEach { $0() } } } // state
//    var c: [() -> ()] = []  // callbacks
//    return (
//          state:{ s                            },
//          change:{ let cs = $0.reduce([], {
//              $0 + [$1]
//          }) ;persist(s, changes: cs, with:manager)},
//          reset:{ s = AppState()    ;persist(s, changes:[], with:manager)},
//        updated:{ c = c + [$0] /*add callback*/},
//        destroy:{                                                  }
//    )
//}
//
////MARK: -
//private func persist(_ appstate:AppState, changes:[AppState.Change], with manager:CoreDataManager) {
//    changes.forEach {
//        switch $0 {
//        case .add   (let i): print(i);
//        case .remove(let i): print(i);
//
//        }
//    }
//
//}
//
//private func loadAppState(with manager:CoreDataManager) -> AppState {
//    return AppState()
//}
//
//private func destroyStore(with manager:CoreDataManager) {
//}
