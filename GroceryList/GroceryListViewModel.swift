//
//  GroceryListViewModel.swift
//  GroceryList
//
//  Created by vikingosegundo on 08/02/2023.
//

import Foundation
import SwiftUI

final class GroceryListViewModel:ObservableObject {
    @Published var items: [ListItem] = []
    
    init(store:Store<AppState,AppState.Change>, roothandler r: @escaping(Message) -> ()) {
        roothandler = r
        store.updated { self.process(store.state()) }
        process(store.state())
    }
    func handle(msg:Message) { DispatchQueue.main.async {/* appstate independent messages*/ } }
    func add   (_ i:ListItem) { roothandler(.groceries(.cmd(.add(item:i)))) }

    func process(_ appState:AppState) {
        DispatchQueue.main.async {
            self.items = appState.groceryList.items
        }
    }
    private let roothandler:(Message) -> ()

}
