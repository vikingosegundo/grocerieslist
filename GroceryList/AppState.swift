//
//  AppState.swift
//  ELPModels
//
//  Created by Manuel Meyer on 11.08.22.
//

struct AppState:Codable {
    public enum Change {
        case add   (ListItem)
        case remove(ListItem)

    }
    public let groceryList:GroceryList
    public init() { self.init(GroceryList()) }
    public func alter(by changes:[Change]  ) -> Self { changes.reduce(self) { $0.alter(by:$1) } }
    public func alter(by changes: Change...) -> Self { changes.reduce(self) { $0.alter(by:$1) } }

}
private extension AppState {
    init(_ l:GroceryList) { groceryList = l }
    func alter(by change:Change) -> Self {
        switch change {
        case let .add   (t): return Self(groceryList.alter(.add   (.item(t))))
        case let .remove(t): return Self(groceryList.alter(.delete(.item(t))))
        }
    }
}
