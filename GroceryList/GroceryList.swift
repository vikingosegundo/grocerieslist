//
//  GroceryList.swift
//  GroceryList
//
//  Created by vikingosegundo on 08/02/2023.
//

import Foundation

struct GroceryList:Codable {
    enum Change {
        case add(Add); enum Add {
            case item(ListItem)
        }
        
        case delete(Delete); enum Delete {
            case item(ListItem)
        }
    }
    public  func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
    private func alter(_ c:Change   ) -> Self {
        switch c {
        case .add(.item(let i)): return Self(items + [i])
        case .delete(.item(let i)): return Self(items.filter{ $0.id != i.id })
            
        }
    }
    init() {
        self.init([])
    }
    init(_ i:[ListItem]) {
        items = i
    }
    let items: [ListItem]
}
struct ListItem: Identifiable,Codable {
    enum Change {
        case text(String)
        case completed(Bool)
    }
    let id: UUID
    let text:String
    let completed:Bool
    
    init() {
        self.init(UUID(), "", false)
    }
    
    private
    init(_ i: UUID,_ t: String,_ c: Bool) {
        id        = i
        text      = t
        completed = c
    }
    public  func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
    private func alter(_ c:Change   ) -> Self {
        switch c {
        case .text     (let t): return Self(id, t   , completed)
        case .completed(let c): return Self(id, text, c        )
        }
    }
}
