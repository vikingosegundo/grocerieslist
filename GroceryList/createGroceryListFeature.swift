//
//  createGroceryListFeature.swift
//  GroceryList
//
//  Created by vikingosegundo on 08/02/2023.
//

import Foundation

func createGroceryListFeature(
    store    s: Store<AppState,AppState.Change>,
    output out: @escaping Output
) -> Input {
    func execute(cmd:Message.Groceries.CMD) {
        switch cmd {
        case .add(item: let i): s.change(.add(i))
        case .remove(item: let i): s.change(.remove(i))

        }
    }
    
    return {
        if case let .groceries(.cmd(c)) = $0 { execute(cmd:c) }
    }
}
