//
//  ContentView.swift
//  GroceryList
//
//  Created by vikingosegundo on 08/02/2023.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var groceries: GroceryListViewModel
    @State private var presentTitleInput = false
    @State private var enteredText = ""

    var body: some View {
        VStack {
            NavigationView {
                
                List {
                    ForEach(groceries.items) {
                        Text($0.text)
                    }
                }.toolbar { Button { askTextForNewItem() } label: { Image(systemName:"plus") } }
            }
        }
        .padding()
        
        .alert("New Item",
               isPresented: $presentTitleInput,
               actions: {
            TextField("text", text:$enteredText)
            Button               {   save() } label: { Text("Add Item") }
            Button(role:.cancel) { cancel() } label: { Text("Cancel"  ) } },
               message: { Text("Please enter text for new Item.") }
        )
    }
}

private extension ContentView {
    func               save() { saveToList()             }
    func askTextForNewItem() { presentTitleInput = true }
    func             cancel() { reset()                  }
    func              reset() { enteredText = ""        }
}
private extension ContentView {
    func saveToList() {
        let item:ListItem = .init().alter(.text(enteredText.trimmed()))
        !enteredText.isEmpty()
        ? groceries.add(item)
        : ()
        reset()
    }
}
