//
//  GroceryListSpecifications.swift
//  GroceryListSpecifications
//
//  Created by vikingosegundo on 08/02/2023.
//

import Quick
import Nimble
@testable import GroceryList


final class AppStateSpecifications: QuickSpec {
    override func spec() {
        describe("AppState") {
            let s = AppState()
            context("creation") {
                it("grocerylist exists"      ) { expect(s.groceryList      ).toNot(beNil())}
                it("grocerylist has no items") { expect(s.groceryList.items).to   (haveCount(0))}
            }
        }
    }
}
final class GroceryListSpecifications: QuickSpec {
    override func spec() {
        describe("GroceryList") {
            let groceryList = GroceryList()
            
            context("creation") {
                it("has no items") { expect(groceryList.items).to(haveCount(0)) }
            }
            context("add Item") {
                let groceryList0 = groceryList.alter(.add(.item(.init().alter(.text("an Item")))))
                it("has one items") {expect(groceryList0.items).to(haveCount(1))}
                it("has one items with correct text") {expect(groceryList0.items.first!.text).to(equal("an Item"))}
                context("remove Item") {
                    let item = groceryList0.items.first!
                    let groceryList1 = groceryList.alter(.delete(.item(item)))
                    it("has no items") {expect(groceryList1.items).to(haveCount(0))}
                }
            }
        }
    }
}
